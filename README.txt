This module provides a "Compressed filefield" widget for fields of type file 
that allow to archive and compress files attached to an entity into one single
file. So in principle this filefield widget can gather both files and images 
from other fields of type file and image.

Similar project and how this is different:

This module is a spin-off of the original File Compressor field module. Big 
thanks to plopesc! The main reason for the spin-off is to have an original
filefield instead of another module field. This has several advantages:

 - Rules can copy the file to other filefields because now they are the same
   datatype. Important for example Commerce File.
 - Integrates "almost" flawlessly with File (Field) Paths to rename the path 
   and/or filename.
 - Can generate multiple different compression type files on the same field.



Installation
---------------

1. Follow the normal Drupal module installation procedures.


Usage
---------------

Usage is straightforward. Add a File field with widget "Compressed file" to any 
entity:
 - Select the Fields you want to include in the generated compressed file(s).
 - Select which Compression types you want to have generated inside your field.
 - Check or uncheck the Hide widget checkbox to your liking.

Configure the field visibility in the "Manage Display" tab.

Create a new entity, upload attachments for the fields selected above and save.

If you made the File field visible, you will be able to download the compressed
file(s).

Note: By default, Compressed filefield widget module only gives support for File
and Image fields. Besides of that, provides hooks to extend it to other file 
based fields.


Requirements
---------------

This module provides by default two Compressor plugins, Zip and GZip. This list
can be extended using hooks. Each zip provider requires a different PHP library:
- Zip: php_zip extension. [PHP Zip Doc](http://php.net/manual/es/book.zip.php)
- GZip: Archive Tar PEAR package
[Archive Tar Doc](http://pear.php.net/package/Archive_Tar)

Authors/Credits
---------------

* Original author: [plopesc](http://drupal.org/user/282415)
* Spin-Off created by: [alexverb] (https://www.drupal.org/u/alexverb)