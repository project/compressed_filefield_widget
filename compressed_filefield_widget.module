<?php

/**
 * Implements hook_field_widget_info().
 */
function compressed_filefield_widget_field_widget_info() {
  return array(
    'compressed_filefield_widget' => array(
      'label' => t('Compressed filefield'),
      'field types' => array('file'),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'compressed_filefield_widget' => array(
          'compressed_fields' => array(),
          'file_compression' => array(
            'zip_built_in' => 'zip_built_in',
            'gzip' => 0,
          ),
          'hide_widget' => 1,
        ),
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function compressed_filefield_widget_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  // This is a file widget, plus extra options.
  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $settings = $instance['widget']['settings']['compressed_filefield_widget'];

  foreach (element_children($elements) as $delta) {
    // If not using custom extension validation, ensure this is a zip.
    $elements[$delta]['#upload_validators']['file_validate_extensions']= array('zip');
  }

  return $elements;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function compressed_filefield_widget_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if ($form['#instance']['widget']['type'] == 'compressed_filefield_widget') {
    
    // Set field to not required and hide the checkbox.
    $form['instance']['required']['#default_value'] = FALSE;
    $form['instance']['required']['#access'] = FALSE;
    
    // Hide the cardinality setting on the field settings for properties fields.
    $form['field']['cardinality']['#access'] = FALSE;

    // Unset settings fields we do not need.
    unset($form['instance']['settings']['max_filesize']);
    unset($form['instance']['settings']['description_field']);
    unset($form['instance']['settings']['file_extensions']);

    // This field needs to be present otherwise token module will complain.
    $form['instance']['description']['#access'] = FALSE;

    // Change the cardinality of the field to the number of compressed files.
    $form['#submit'][] = 'compressed_filefield_widget_form_field_ui_field_edit_form_submit';
  }
}

/**
 * Submit function to change cardinality of the field to the number of
 * compressed files.
 * 
 * @see compressed_filefield_widget_form_field_ui_field_edit_form_alter()
 */
function compressed_filefield_widget_form_field_ui_field_edit_form_submit($form, &$form_state) {
  $compressions = $form_state['values']['instance']['widget']['settings']['compressed_filefield_widget']['file_compression'];
  $count = 0;
  foreach($compressions as $compression => $enabled) {
    if ($enabled) {
      $count++;
    }
  }
  $form_state['values']['field']['cardinality'] = $count;
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function compressed_filefield_widget_field_widget_compressed_filefield_widget_form_alter(&$element, &$form_state, $context) {
  // Hide our compressed filefield since it's automatically populated.
  $element['#access'] = empty($context['instance']['widget']['settings']['compressed_filefield_widget']['hide_widget']) ? TRUE : FALSE;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function compressed_filefield_widget_field_widget_settings_form($field, $instance) {
  $widget_settings = $instance['widget']['settings'];
  $valid_fields = array();
  $form = array();

  $entity_fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  $all_fields = field_info_fields();
  $supported_field_types = module_invoke_all('compressed_filefield_widget_type_info');

  // Get a list of all valid fields that we both support
  // and are part of this entity.
  foreach ($all_fields as $this_field) {
    if (array_key_exists($this_field['field_name'], $entity_fields)) {
      if (in_array($this_field['type'], array_keys($supported_field_types)) && ($this_field['field_name'] != $field['field_name'])) {
        $valid_fields[$this_field['field_name']] = $entity_fields[$this_field['field_name']]['label'];
      }
    }
  }

  $form['compressed_filefield_widget']['compressed_fields'] = array(
    '#title' => t('Fields to compress'),
    '#type' => 'checkboxes',
    '#options' => $valid_fields,
    '#default_value' => $widget_settings['compressed_filefield_widget']['compressed_fields'],
  );
  
  $providers = compressed_filefield_widget_load_provider_info();
  $options = array_map(function($item){
    return $item['name'];
  }, $providers);

  $form['compressed_filefield_widget']['file_compression'] = array(
    '#title' => t('File compression'),
    '#description' => t('Select what type of compressed files you want to generate.'),
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#options' => $options,
    '#default_value' => $widget_settings['compressed_filefield_widget']['file_compression'],
  );
  
  $form['compressed_filefield_widget']['hide_widget'] = array(
    '#title' => t('Hide widget'),
    '#description' => t('Enable this if you do not want users to alter the field.'),
    '#type' => 'checkbox',
    '#default_value' => $widget_settings['compressed_filefield_widget']['hide_widget'],
  );

  return $form;
}

/**
 * Implements hook_compressed_filefield_widget_field_type_info().
 */
function compressed_filefield_widget_compressed_filefield_widget_type_info() {
  return array(
    'image' => array(
      'column' => 'fid',
    ),
    'file' => array(
      'column' => 'fid',
    ),
  );
}

/**
 * Retrieves File Compressor field provider object.
 *
 * @param $provider_id
 *   The provider_id to load.
 *
 * @return CompressedFilefieldWidgetProviderInterface|bool
 *   The provider object, FALSE otherwise.
 */
function compressed_filefield_widget_load_provider($provider_id) {
  $provider_info = compressed_filefield_widget_load_provider_info($provider_id);
  if ($provider_info) {
    $class_name = $provider_info['class'];
    module_load_include('php', 'compressed_filefield_widget', 'includes/CompressedFilefieldWidgetProviderInterface');
    include_once $provider_info['path'] . '/' . $provider_info['class'] . '.php';

    if (class_exists($class_name)) {
      $provider = new $class_name();
      if ($provider instanceof CompressedFilefieldWidgetProviderInterface) {
        return $provider;
      }
    }
  }

  return FALSE;
}

/**
 * Loads aggregated provider data.
 *
 * Modules can hook into this with hook_compressed_filefield_widget_provider_info()
 * or hook_compressed_filefield_widget_provider_info_alter().
 *
 * @param string $provider_id
 *   The provider id to load.
 *
 * @return array|bool
 *   The provider info array, or FALSE if it does not exist.
 */
function compressed_filefield_widget_load_provider_info($provider_id = '') {
  $compressed_filefield_widget_providers =& drupal_static(__FUNCTION__);

  if (empty($compressed_filefield_widget_providers)) {
    $compressed_filefield_widget_providers = module_invoke_all('compressed_filefield_widget_provider_info');
    drupal_alter('compressed_filefield_widget_provider_info', $compressed_filefield_widget_providers);
  }

  if (empty($provider_id)) {
    return $compressed_filefield_widget_providers;
  }
  elseif (!empty($compressed_filefield_widget_providers[$provider_id])) {
    return $compressed_filefield_widget_providers[$provider_id];
  }

  return FALSE;
}

/**
 * Implements hook_compressed_filefield_widget_provider_info().
 */
function compressed_filefield_widget_compressed_filefield_widget_provider_info() {
  $providers = array(
    'zip_built_in' => array(
      'name' => t('Zip (Built in)'),
      'class' => 'CompressedFilefieldWidgetProviderZip',
      'path' => drupal_get_path('module', 'compressed_filefield_widget') . '/includes',
    ),
    'gzip_zlib' => array(
      'name' => t('GZip (Zlib)'),
      'class' => 'CompressedFilefieldWidgetProviderGZip',
      'path' => drupal_get_path('module', 'compressed_filefield_widget') . '/includes',
    ),
  );

  return $providers;
}

/**
 * Implements hook_field_attach_presave().
 */
function compressed_filefield_widget_field_attach_presave($entity_type, $entity) {
  $entity_info = entity_get_info($entity_type);
  
  if (isset($entity->{$entity_info['entity keys']['bundle']})) {
    $field_info_instances = field_info_instances($entity_type, $entity->{$entity_info['entity keys']['bundle']});

    foreach($field_info_instances as $instance) {
      if (isset($instance['widget']['type']) && $instance['widget']['type'] == 'compressed_filefield_widget') {
        $field = field_info_field($instance['field_name']) ;
        // Change this to field language later
        $langcode = $entity->language;
        $items = field_get_items($entity_type, $entity, $instance['field_name'], $langcode);

        if (!empty($items)) {
          foreach($items as $delta => $item) {
            $items[$delta] = (array) file_load($item['fid']);
          }
        }


        $supported_field_types = module_invoke_all('compressed_filefield_widget_type_info');
        $langcodes = ($field['translatable']) ? array($langcode) : array_keys(language_list());
        foreach (array_filter($instance['widget']['settings']['compressed_filefield_widget']['compressed_fields']) as $field_to_compress) {
          foreach ($langcodes as $current_langcode) {
            $field_items = field_get_items($entity_type, $entity, $field_to_compress, $current_langcode);
            if (is_array($field_items)) {
              $field_info_field = field_info_field($field_to_compress);
              $column = $supported_field_types[$field_info_field['type']]['column'];
              foreach ($field_items as $field_item) {
                if (isset($field_item[$column]) && $field_item[$column] > 0) {
                  $current_file = file_load($field_item[$column]);
                  $files[$current_file->fid] = $current_file->uri;
                }
              }
            }
          }
        }
        $delta = 0;
        $compression_providers = $instance['widget']['settings']['compressed_filefield_widget']['file_compression'];
        // Thus needs a lot of refractoring still.
        $items = array();
        foreach ($compression_providers as $compressor_provider => $enabled) {
          if ($enabled) {
            if (!empty($files) && $file = compressed_filefield_widget_generate_compressed_file($entity_type, $entity, $field, $instance, $langcode, $items, $files, $compressor_provider)) {
              $entity->{$instance['field_name']}[$langcode][$delta] = (array) $file;
              $entity->{$instance['field_name']}[$langcode][$delta]['display'] = $field['settings']['display_field'];
            }
          }
          else {
            // Remove any file that might have been saved beforehand.
            unset($entity->{$instance['field_name']}[$langcode][$delta]);
          }
          $delta++;
        }
      }
    }
  }
}

/**
 * Generates compressed file when saving field.
 *
 * @param $entity_type
 *   The type of $entity.
 * @param $entity
 *   The entity for the operation.
 * @param $field
 *   The field structure for the operation.
 * @param $instance
 *   The instance structure for $field on $entity's bundle.
 * @param $langcode
 *   The language associated with $items.
 * @param $items
 *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
 * @param $files
 *   Files to archive.
 *
 * @return bool|\stdClass
 *   Compressed file entity or FALSE if something fails.
 */
function compressed_filefield_widget_generate_compressed_file($entity_type, $entity, $field, $instance, $langcode, $items = array(), $files = array(), $compressor_provider) {
  $compressor = compressed_filefield_widget_load_provider($compressor_provider);
  $create_compressed_file = empty($items) || !empty($entity->revision);
  if ($create_compressed_file) {
    $directory_uri = file_field_widget_uri($field, $instance);
    if (!file_prepare_directory($directory_uri, FILE_CREATE_DIRECTORY)) {
      drupal_set_message('The directory wasn\'t created or was not writable', 'error');
      return FALSE;
    }
    do {
      $slash = empty($instance['settings']['file_directory']) ? '' : '/';
      $file_uri = $compressor->generateCompressedFileUri($directory_uri . $slash . 'compressed_file_' . user_password(4));
      $exists = file_exists($file_uri);
    } while ($exists);
  }
  else {
    $compressed_field_components = $items['0'];
    $file_uri = $compressed_field_components['uri'];
  }

  if ($compressor->generateCompressedFile($file_uri, $files)) {
    global $user;
    $file = new stdClass();
    $file->fid = NULL;
    $file->uri = $file_uri;
    $file->uid = $user->uid;
    $file->filename = drupal_basename($file_uri);
    $file->status = FILE_STATUS_PERMANENT;
    // If we are replacing an existing file re-use its database record.
    if (!$create_compressed_file) {
      $file->fid = $compressed_field_components['fid'];
      $file->filename = $compressed_field_components['filename'];
    }
    $file = file_save($file);

    if ($create_compressed_file) {
      drupal_register_shutdown_function('compressed_filefield_widget_store', $entity_type, $field, $instance, $langcode, $file, $compressor_provider);
    }
    return $file;
  }
  return FALSE;
}


/**
 * Moves File Compressor field file to its definitive path.
 *
 * @param $entity_type
 *   The entity type name.
 * @param $field
 *   The field definition array.
 * @param $instance
 *   The instance definition array.
 * @param $langcode
 *   The specific langcode.
 * @param $file
 *   The file object to move.
 */
function compressed_filefield_widget_store($entity_type, $field, $instance, $langcode, $file, $compressor_provider) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type)
    ->fieldCondition($field['field_name'], 'fid', $file->fid);
  $result = $query->execute();

  if (!empty($result[$entity_type])) {
    $compressor = compressed_filefield_widget_load_provider($compressor_provider);
    $entity = entity_load($entity_type, array(key($result[$entity_type])));
    $directory_uri = file_field_widget_uri($field, $instance);
    list($id, $vid,) = entity_extract_ids($entity_type, reset($entity));
    $file_name_components = isset($vid) ? array($entity_type, $id, $vid) : array($entity_type, $id);
    if ($field['translatable']) {
      $file_name_components[] = $langcode;
    }
    $slash = empty($instance['settings']['file_directory']) ? '' : '/';
    $destination = $directory_uri . $slash . implode('-', $file_name_components);
    $destination = $compressor->generateCompressedFileUri($destination);
    if ($destination = file_unmanaged_move($file->uri, $destination, FILE_EXISTS_RENAME)) {
      $file->uri = $destination;
      $file->filename = drupal_basename($destination);
      file_save($file);
      cache_clear_all("field:$entity_type:$id", 'cache_field');
    }
  }
}

/**
 * Implements hook_token_info().
 */
function compressed_filefield_widget_token_info() {

  $info['tokens']['file']['cfw-extension'] = array(
    'name' => t('Extension'),
    'description' => t('The compressed filefield widget extension.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function compressed_filefield_widget_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  // File tokens.
  if ($type == 'file' && !empty($data['file'])) {
    $file = $data['file'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'cfw-extension':
          $provider_info = compressed_filefield_widget_compressed_filefield_widget_provider_info();
          $double_extensions = array();
          foreach($provider_info as $provider_id => $info) {
            $compressor = compressed_filefield_widget_load_provider($provider_id);
            $double_extensions[] = $compressor->getExtension();
          }
          $double_extensions = implode('|', $double_extensions);
          // Check if one of the double extensions should be returned.
          if (preg_match('/(?:' . $double_extensions . ')$/', $file->origname, $matches)) {
            $cfw_extension = $matches[0];
            $replacements[$original] = $cfw_extension;
          }
          // Otherwise handle as a normal file.
          else {
            $cfw_extension = pathinfo($file->origname, PATHINFO_EXTENSION);
            $replacements[$original] = $cfw_extension;
          }
          break;
      }
    }
  }

  return $replacements;
}
